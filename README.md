# README #

### What is this repository for? ###

* Movie management tool implemented using AngularJS 1.5
* 1.0.0.0

### How do I get set up? ###

* This tool does not use any databases as the main goal of the project is to get some exposure in Angular. But there is a service created for handling movies in memory.