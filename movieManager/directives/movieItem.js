﻿myMovieApp.directive('movieItem', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div style="overflow: auto; width:600px; padding:10px; margin:10px; border:1px solid black;">' +
                    '<div style="float: right; height: 50px ;width: 250px">' +
                         '<star-rating rating-value="movie.Rating" data-max="10" ></star-rating>' +
                    '</div>' +
                    '<h2>{{movie.Title}} ({{movie.Year}})</h2>' +
                    '<p>{{movie.Description}}</p>' +
                    '<button style="float: right;" ng-click="loadDetail(movie.Title)">Detail</button>' +
                   '</div>'
    }
});
