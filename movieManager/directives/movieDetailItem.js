﻿myMovieApp.directive('movieDetailItem', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div style="overflow: auto;width: 650px ; ">' +
                    '<div style="float: right; height: 50px ;width: 300px">' +
                         'Rating &nbsp &nbsp ' +
                         '<star-rating rating-value="movie.Rating" data-max="10" ></star-rating>' +
                    '</div>' +
                    '<h2>{{movie.Title}}</h2>' +
                    '<p>{{movie.Description}}</p> <br/>' +
                    'Directed by : {{movie.Director}} <br/>' +
                    'Release year : {{movie.Year}} <br/>' +
                    'Language : {{movie.Language}} <br/>' +
                    '<div ng-hide="!movie.Characters.length">' +
                        '<h2>Characters</h2>' +
                        '<ul>' +
                            '<li ng-repeat="character in movie.Characters ">{{character.Character}}({{character.Actor}})' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '</div>'

    }
});