﻿myMovieApp.service('movieService', function () {
    var movieList = [];

    var addMovie = function (movie) {
        movie.Characters = [];
        movieList.push(movie);
    };

    var getMovies = function () {
        return movieList;
    };

    var getLastMovie = function() {
        return movieList[movieList.length - 1];
    }

    var getMovie = function (title) {

        for (var i = 0; i < movieList.length; i++) {
            var movie = movieList[i];
            if (movie.Title === title) {
                return movie;
            }
        }
        return undefined;
    }

    var addCharacter = function (movieTitle, character) {
        var movie = getMovie(movieTitle);

        if (movie != undefined) {
            movie.Characters.push(character);
        }
    };

    var getCharacters = function (movieTitle) {
        var movie = getMovie(movieTitle);

        if (movie != undefined) {
            return movie.Characters;
        }
        return undefined;
    };

    return {
        addMovie: addMovie,
        getMovies: getMovies,
        getLastMovie: getLastMovie,
        getMovie: getMovie,
        addCharacter: addCharacter,
        getCharacters: getCharacters
    };

});