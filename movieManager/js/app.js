﻿var myMovieApp = angular.module('movieApp', ['ngRoute']);
myMovieApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/createmovie', {
            templateUrl: 'view/createmovie.html',
            controller: 'createMovieController'
        })
    .when('/details/:title?', {
        templateUrl: 'view/moviedetail.html',
        controller: 'movieDetailController'
    })
    .when('/characters', {
        templateUrl: 'view/characters.html',
        controller: 'charactersController'
    })
    .when('/mymovies', {
        templateUrl: 'view/mymovies.html',
        controller: 'myMoviesController'
    });
}
]);