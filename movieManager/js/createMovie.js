﻿
myMovieApp.controller('createMovieController', function ($scope, $location, $rootScope, movieService) {

    $scope.create = function (title, description, director, year, rating, language) {

        var movieInfo = {
            Title: title,
            Description: description,
            Director: director,
            Year: year,
            Rating: rating,
            Language: language
        }
        movieService.addMovie(movieInfo);

        $location.path("/characters");
    }
});