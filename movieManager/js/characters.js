﻿myMovieApp.controller('charactersController', function ($scope, movieService, $location) {
    $scope.characters = [];
    $scope.movieTitle = movieService.getLastMovie().Title;
    $scope.add = function (characterName, actor) {

        var character = {
            Character: characterName,
            Actor: actor
        }
        $scope.characters.push(character);
        movieService.addCharacter($scope.movieTitle, character);

        $scope.character = "";
        $scope.actor = "";
    }

    $scope.go = function (path) {
        $location.path(path);
    }
});