﻿myMovieApp.controller('movieDetailController', function ($scope, $location, $routeParams, movieService) {
    
    $scope.movie = movieService.getMovie($routeParams.title);

    $scope.go = function (path) {
        $location.path(path);
    }
});