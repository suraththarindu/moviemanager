﻿myMovieApp.controller('myMoviesController', function ($scope, $rootScope, $location, movieService) {
    $scope.movies = movieService.getMovies();

    $scope.go = function (path) {
        $location.path(path);
    }

    $scope.loadDetail = function (movieTitle) {
        $location.path('/details/').search({ title : movieTitle });
    }
});